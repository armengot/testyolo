#include <iostream>
#include <fstream>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <iomanip>
#include <chrono>

int main(int argc, char* argv[])
{
    cv::Mat frame;
	std::cout << "Reading video" << std::endl;
	cv::VideoCapture cap;
    int limit = 60;
    bool record = true;

	/* offline video */
	if (argc==2)
		cap = cv::VideoCapture(argv[1]);
	else
		cap = cv::VideoCapture(0); /* web cam */	
	/* mobile camera with: "IP Webcam" android App*/
	//cv::VideoCapture cap("http://192.168.1.41:8080/video");
	//cv::VideoCapture cap("http://100.168.1.135:8080/video");
	if (!cap.isOpened()) 
	{
		std::cout << "Cannot open VIDEO." << std::endl;
		return(-1);
	}
    auto start = std::chrono::high_resolution_clock::now();
    int nframe = 0;
    while (record)
    {
        cap >> frame;
		std::stringstream filename;
		filename << "FRAME_" << std::setfill('0') << std::setw(5) << nframe << ".jpg";
		cv::imwrite(filename.str(), frame);	        
        nframe = nframe + 1;
        auto t = std::chrono::high_resolution_clock::now();
        auto lapse = std::chrono::duration_cast<std::chrono::seconds>(t - start).count();
        if (lapse>limit)
            record = false;
        std::cout << "\rFRAME: (" << nframe << ") " << lapse << "seconds." << std::flush;
    }
    std::cout << std::endl;
    /* ffmpeg -framerate 30 -i FRAME_%05d.jpg -c:v libx264 -r 30 output.mp4 */

	cap.release();
	cv::destroyAllWindows();
	return(0);
}
