# Test YOLO
Modern Neural Network architectures known as [YOLO](https://pjreddie.com/darknet/yolo/) ("You Only Look Once") have evolved in recent years to become a 'de facto' standard.

My interest in testing them was primarily to explore their potential. I began by trying YOLO(v4) from its original repository and was so pleased with the results that I uploaded a demonstrative [video](https://www.tiktok.com/@marceloarmengot/video/7255318894305381659) on TikTok.

From a real-time processing perspective, considering 30 frames per second rate, each new frame will take 33 milliseconds to arrive. So, if your object detection takes longer than that, real-time detection becomes impractical.

Certainly, processing capability heavily relies on the GPU, but it's undeniable that successive versions of YOLO released so far have made improvements in both detection capacity (more detections) and speed (faster processing). This led me to use the same code (with some modifications) to test versions 5 and beyond. While I encountered challenges with converting models from version 7, I believe version 8 surpasses them.

Here are some basic C++ code examples to try YOLO versions 4, 5, and 8. I hope these examples will help you easily incorporate real-time object detection capabilities into your projects.

## YOLO (v8)

There are some versions from YOLO and many people using YOLO models to impreove object detection and tracking.
The best I found was the development released by [Muhammad Rizwan Munawar](https://www.linkedin.com/in/muhammadrizwanmunawar/) in his GitHub [repo](https://github.com/RizwanMunawar/yolov8-object-tracking)

### Export from PT to ONNX
Using ultralytics scripts (links below):
```
$ python3 export.py --weights yolov8m.pt --include onnx --opset 12
```
Or:
```
$ python3
>>> from ultralytics import YOLO
>>> model = YOLO("models/v8/yolov8m.yaml")
>>> model = YOLO("yolov8m.pt")
>>> path = model.export(format="onnx",opset=12)
```

## YOLO (first v4) example
![TEST](samples/results/sample.gif)

## Comparisons

Another complete 1.5min video testing YOLO versions v4 vs v8 available in my personal [tiktok](https://www.tiktok.com/@marceloarmengot/video/7257581224565148955).

### Behaviour

- Original (v4) YOLO vs (v5):

![TEST4](samples/results/YOLOv4.gif)![TEST5](samples/results/YOLOv5.gif)

- Original (v4) YOLO vs (v8):

![TEST4](samples/results/YOLOv4.gif)![TEST5](samples/results/YOLOv8.gif)

### Time

Tested with a GPU NVIDIA card (Quadro T500 Mobile):

| Version  | GPU Inference | Credits |
| -------- | ------------- | -----------------------|
| YOLOv4   | 144ms      | [Bochkovskiy et al.](https://arxiv.org/pdf/2004.10934.pdf) |
| YOLOv5   | 90-110ms   | [Jocher Glenn](https://orcid.org/0000-0001-5950-6979) |
| YOLOv7   | ~41-42ms   | [Bochkovskiy et al.](https://github.com/WongKinYiu/yolov7) |
| YOLOv8   | ~40ms      | [Jocher Glenn](https://orcid.org/0000-0001-5950-6979) |
| YOLOv8b  | ~230ms     | [Muhammad Rizwan Munawar](https://github.com/RizwanMunawar/yolov8-object-tracking)


## Requirements
What you need:
- GPU in your hardware.
- [Install](https://gist.github.com/raulqf/f42c718a658cddc16f9df07ecc627be7) OpenCV with CUDA support in your software.
- Check the ```CMakeLists.txt``` and edit with your local path.

## Test
Easy:
```
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ./testyolo4
$ ./testyolo5
$ ./testyolo8
```

## Options
Video as parameter:
- If webcam use:
```
$ ./testyolo5
```
- To process any video:
```
$ ./testyolo5 video.mp4
```
- Can use "IP Webcam" android App with your mobile camera and render real time video:
```
cv::VideoCapture cap("http://192.168.1.41:8080/video");
```
- Uncomment last section to save and name frames.
```
std::stringstream filename;
filename << "FRAME_" << std::setfill('0') << std::setw(5) << nframe << ".jpg";
cv::imwrite(filename.str(), frame);	
```

## Other tools
In ```cmake``` file ```rec``` binary tool is built to record frames:
Make videos from frames enabling section as explained above and using ```ffmpeg```:
```
$ ffmpeg -framerate 25 -i FRAME_%03d.jpg -c:v libx264 -r 30 output.mp4
```
Or make GIFs to simplify show:
```
$ ffmpeg -r 30 -f image2 -start_number 0 -i FRAME_%05d.jpg -vf "fps=30,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 YOLOv8.gif
```

## Links of interest
All I needed:
- Main [paper](https://arxiv.org/pdf/1506.02640.pdf).
- Recommended [reading](https://medium.com/deelvin-machine-learning/the-evolution-of-the-yolo-neural-networks-family-from-v1-to-v7-48dd98702a3d).
- Original [repo](https://github.com/AlexeyAB/darknet) for more info.
- Differences between YOLO(v5) and YOLO(v8) output: [Bart Justas](https://github.com/JustasBart/yolov8_CPP_Inference_OpenCV_ONNX)
- About v7 from [Bochkovskiy et al.](https://github.com/WongKinYiu/yolov7/blob/main/paper/yolov7.pdf)
- Exporting ```pt``` to ```onnx```: [ultralytics](https://github.com/ultralytics/ultralytics)