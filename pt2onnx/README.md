# Export

To use this repository sources, you will need the Networks as ```.onnx``` formats:

## YOLO (v5)

From YOLOv5 models are saved in PyTorch (```.pt```) format. Made the conversion was easy following the [first](https://learnopencv.com/object-detection-using-yolov5-and-opencv-dnn-in-c-and-python/#YOLOv5-MODEL-CONVERSION) instructions I found with Google help.

```
$ git clone https://github.com/ultralytics/YOLOv5
$ cd YOLOv5
$ pip install -r requirements.txt
$ pip install onnx
$ wget https://github.com/ultralytics/YOLOv5/releases/download/v6.1/YOLOv5s.pt
$ python3 export.py --weights YOLOv5s.pt --include onnx --opset 12
```

## YOLO (v8)
What I did following steps from Ultralytics repo:
```
$ git clone https://github.com/ultralytics/ultralytics
$ cd ultralytics
$ python
>>> from ultralytics import YOLO
>>> model = YOLO("yolov8n.pt")
>>> model.export(format='onnx',imgsz=640,opset=12)
```



