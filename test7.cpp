#include <iostream>
#include <fstream>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/dnn.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <iomanip>
#include <palette.h>
#include <chrono>
#include <vector>

/* global limit */
float THRESHOLD = 0.3;
const float INPUT_WIDTH = 640.0;
const float INPUT_HEIGHT = 640.0;

cv::Mat format_yolov5(const cv::Mat &source) 
{
    int col = source.cols;
    int row = source.rows;
    int _max = MAX(col, row);
    cv::Mat result = cv::Mat::zeros(_max, _max, CV_8UC3);
    source.copyTo(result(cv::Rect(0, 0, col, row)));
    return result;
}

int main(int argc, char* argv[])
{
	std::string modelpath;		
    modelpath="../YOLO7/";	
	std::string names = modelpath + "/coco.names";
	// std::string config = modelpath + "/yolov8.yaml";
	std::string weights = modelpath + "/yolov7.onnx";   
    
	std::vector<cv::Mat> detections;
    
	/* labels load */
	std::cout << " * * * \nLoading  names  from: " << names << std::endl;
	std::vector<std::string> labels;
	std::ifstream ifs(names.c_str());
	std::string line;
	while (std::getline(ifs, line)) 
	{
		labels.push_back(line);
	}

	/* model */ 
	// std::cout << "Loading  config from: " << config << std::endl;
	std::cout << "Loading weights from: " << weights << std::endl;
	auto start = std::chrono::high_resolution_clock::now();
    cv::dnn::Net net;
    try
    {        
        net = cv::dnn::readNet(weights);
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return(0);
    }
    
    
    try 
    {   /* try GPU */
        net.setPreferableBackend(cv::dnn::DNN_BACKEND_CUDA);
        net.setPreferableTarget(cv::dnn::DNN_TARGET_CUDA_FP16);    
    }
    catch(const std::exception& e)
    {   /* else CPU */
        std::cerr << DEBUG_REDTEXT << "Alternative CPU way: " << e.what() << DEBUG_RESETXT << std::endl;
        net.setPreferableBackend(cv::dnn::DNN_BACKEND_OPENCV);
        net.setPreferableTarget(cv::dnn::DNN_TARGET_CPU);
    }
    auto t = std::chrono::high_resolution_clock::now();
	std::cout << DEBUG_BLUETXT << std::chrono::duration_cast<std::chrono::milliseconds>(t - start).count() << "ms loading net" << DEBUG_RESETXT << std::endl;
	std::cout << "NET upload" << std::endl;
	
	std::cout << "Reading video" << std::endl;
	cv::VideoCapture cap;

	/* offline video */
	if (argc==2)
		cap = cv::VideoCapture(argv[1]);
	else
		cap = cv::VideoCapture(0); /* web cam */	
	
    /* mobile camera with: "IP Webcam" android App*/
	//cv::VideoCapture cap("http://192.168.1.41:8080/video");
	//cv::VideoCapture cap("http://100.168.1.135:8080/video");


	if (!cap.isOpened()) 
	{
		std::cout << "Cannot open VIDEO." << std::endl;
		return(-1);
	}
	int nframe = 0;
	while (true) 
	{
		cv::Mat frame,preframe,blob;		
		cap >> preframe;		
		std::cout << "FRAME(" << nframe << "): ";
		std::cout << DEBUG_GREENXT << preframe.size() << DEBUG_RESETXT << std::endl;		
		/* FRAME -> blob */
        frame = format_yolov5(preframe);
        /* Blob to Neural Netwok*/
        start = std::chrono::high_resolution_clock::now();		
        cv::dnn::blobFromImage(frame, blob, 1./255., cv::Size(INPUT_WIDTH, INPUT_HEIGHT), cv::Scalar(), true, false);
        net.setInput(blob);        
        net.forward(detections, net.getUnconnectedOutLayersNames());               
		t = std::chrono::high_resolution_clock::now();
		std::cout << DEBUG_BLUETXT << std::chrono::duration_cast<std::chrono::milliseconds>(t - start).count() << "ms NN work => " << DEBUG_RESETXT;
		auto start2 = std::chrono::high_resolution_clock::now();

        int rows = detections[0].size[2];
        int dimensions = detections[0].size[1];
        detections[0] = detections[0].reshape(1, dimensions);
        cv::transpose(detections[0], detections[0]);
        float *data = (float *)detections[0].data;
        float x_factor = frame.cols / INPUT_WIDTH;
        float y_factor = frame.rows / INPUT_HEIGHT;

        std::vector<int> class_ids;
        std::vector<float> confidences;
        std::vector<cv::Rect> boxes;

        for (int i = 0; i < rows; i++)
        {
            float *scores = data+4;
            cv::Mat scores_vector(1, labels.size(), CV_32FC1, scores);
            cv::Point sort_id;
            double nnmax;
            cv::minMaxLoc(scores_vector, 0, &nnmax, 0, &sort_id);
            
            if (nnmax>THRESHOLD)
            {
                int id = sort_id.x;
                std::string detname = labels[id];

                if (id>=0)
                {
                    float x = data[0];
                    float y = data[1];
                    float w = data[2];
                    float h = data[3];
                    int left = int((x - 0.5 * w) * x_factor);
                    int top = int((y - 0.5 * h) * y_factor);
                    int width = int(w * x_factor);
                    int height = int(h * y_factor);
                    cv::Rect rect(left, top, width, height);                
                    cv::Scalar color(palette[id][2], palette[id][1], palette[id][0]);
                    cv::rectangle(frame, rect, color, 2);                    
                    cv::putText(frame, detname, cv::Point(rect.x, rect.y - 5), cv::FONT_HERSHEY_SIMPLEX, 1, color, 1);
                    std::cout << " [" << id << "|" << detname << "|" << int(nnmax*10000)/100 << "%]";
                }
            }

            data = data + dimensions;

        }
        std::cout << std::endl;    	
        cv::Scalar color(0,255,0);
		cv::putText(frame, "YOLOv8", cv::Point(4, 40), cv::FONT_HERSHEY_DUPLEX, 1, color, 1.5);        

        cv::Rect original_dim(0,0,preframe.cols,preframe.rows);
		/* uncomment to save frames */
        
        /*
		std::stringstream filename;
		filename << "FRAME_" << std::setfill('0') << std::setw(5) << nframe << ".jpg";
        cv::Mat out = frame(original_dim);
		cv::imwrite(filename.str(), out);				        
        */
        

		auto t2 = std::chrono::high_resolution_clock::now();
		std::cout << DEBUG_BLUETXT << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - start2).count() << "ms of CPU time" << DEBUG_RESETXT << std::endl;
		nframe = nframe + 1;		

		/* after frames generated : render with ffmpeg
			$ ffmpeg -framerate 25 -i FRAME_%03d.jpg -c:v libx264 -r 30 output.mp4
		*/        
		cv::imshow("YOLOv7", frame(original_dim));
		if (cv::waitKey(1) == 'q')
			break;
	}
	cap.release();
	cv::destroyAllWindows();
	return(0);
}
