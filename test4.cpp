#include <iostream>
#include <fstream>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/dnn.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <iomanip>
#include <palette.h>
#include <chrono>

/* global limit */
float THRESHOLD = 0.7;

int main(int argc, char* argv[])
{
	std::string modelpath;	
	modelpath="../YOLO4/";	

	/* YOLO downloaded from Darknet */
	std::string names = modelpath + "/coco.names";
	std::string config = modelpath + "/yolov4.cfg";
	std::string weights = modelpath + "/yolov4.weights";
	std::vector<cv::Mat> detections;

	/* labels load */
	std::cout << " * * * \nLoading  names  from: " << names << std::endl;
	std::vector<std::string> labels;
	std::ifstream ifs(names.c_str());
	std::string line;
	while (std::getline(ifs, line)) 
	{
		labels.push_back(line);
	}

	/* model */ 
	std::cout << "Loading  model  from: " << config << std::endl;
	std::cout << "Loading weights from: " << weights << std::endl;
	auto start = std::chrono::high_resolution_clock::now();
	cv::dnn::Net net = cv::dnn::readNetFromDarknet(config, weights);
	auto t = std::chrono::high_resolution_clock::now();
	std::cout << DEBUG_BLUETXT << std::chrono::duration_cast<std::chrono::milliseconds>(t - start).count() << "ms loading net" << DEBUG_RESETXT << std::endl;
	/*
		net.setPreferableBackend(cv::dnn::DNN_BACKEND_OPENCV);
		net.setPreferableTarget(cv::dnn::DNN_TARGET_CPU);
	*/
	std::cout << "GPU upload" << std::endl;
	start = std::chrono::high_resolution_clock::now();
	net.setPreferableBackend(cv::dnn::DNN_BACKEND_CUDA);
	net.setPreferableTarget(cv::dnn::DNN_TARGET_CUDA);
	t = std::chrono::high_resolution_clock::now();
	std::cout << DEBUG_BLUETXT << std::chrono::duration_cast<std::chrono::milliseconds>(t - start).count() << "ms loading GPU" << DEBUG_RESETXT << std::endl;

	std::cout << "Reading video" << std::endl;
	cv::VideoCapture cap;

	/* offline video */
	if (argc==2)
		cap = cv::VideoCapture(argv[1]);
	else
		cap = cv::VideoCapture(0); /* web cam */	
	/* mobile camera with: "IP Webcam" android App*/
	//cv::VideoCapture cap("http://192.168.1.41:8080/video");
	//cv::VideoCapture cap("http://100.168.1.135:8080/video");


	if (!cap.isOpened()) 
	{
		std::cout << "Cannot open VIDEO." << std::endl;
		return(-1);
	}
	int nframe = 0;
	while (true) 
	{
		cv::Mat frame;		
		cap >> frame;		
		std::cout << "FRAME(" << nframe << "): ";
		std::cout << DEBUG_GREENXT << frame.size() << DEBUG_RESETXT << std::endl;		
		/* FRAME -> blob */
		cv::Mat blob = cv::dnn::blobFromImage(frame, 1.0 / 255, cv::Size(640, 640), cv::Scalar(0, 0, 0), true, false);		
		/* Blob to Neural Netwok*/
		start = std::chrono::high_resolution_clock::now();		
		net.setInput(blob);
		net.forward(detections);        
		t = std::chrono::high_resolution_clock::now();
		std::cout << DEBUG_BLUETXT << std::chrono::duration_cast<std::chrono::milliseconds>(t - start).count() << "ms NN work => " << DEBUG_RESETXT;
		auto start2 = std::chrono::high_resolution_clock::now();		
		for (const cv::Mat& output : detections)
		{
			for (int row = 0; row < output.rows; row++)
			{
				if (output.at<float>(row,4)>THRESHOLD)
				{
					float x = output.at<float>(row, 0) * frame.cols;
					float y = output.at<float>(row, 1) * frame.rows;
					float width = output.at<float>(row, 2) * frame.cols;
					float height = output.at<float>(row, 3) * frame.rows;					
					cv::Rect rect(x - width/2, y - height/2, width, height);			

					int id = -1;
					float tmax = 0.5;
					for (int c = 5; c < output.cols; c++) 
					{
						float confidence = output.at<float>(row, c);
						if (confidence > tmax) 
						{
							tmax = confidence;
							id = c - 5;
						}
					}
					if (id >= 0)
					{
						cv::Scalar color(palette[id][2], palette[id][1], palette[id][0]);
						cv::rectangle(frame, rect, color, 2);
						std::string detname = labels[id];
						cv::putText(frame, detname, cv::Point(rect.x, rect.y - 5), cv::FONT_HERSHEY_SIMPLEX, 1, color, 1);
						std::cout << " [" << id << "|" << detname << "|" << int(output.at<float>(row,4)*10000)/100 << "%]";
					}
				}
			}
		}	
		std::cout << std::endl;    	
		cv::Scalar color(0,255,0);
		cv::putText(frame, "YOLOv4", cv::Point(4, 40), cv::FONT_HERSHEY_DUPLEX, 1, color, 1.5);

		/* uncomment to save frames */
		/*
		std::stringstream filename;
		filename << "FRAME_" << std::setfill('0') << std::setw(5) << nframe << ".jpg";
		cv::imwrite(filename.str(), frame);		
		*/
		auto t2 = std::chrono::high_resolution_clock::now();
		std::cout << DEBUG_BLUETXT << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - start2).count() << "ms of CPU time" << DEBUG_RESETXT << std::endl;
		nframe = nframe + 1;		

		/* after frames generated : render with ffmpeg
			$ ffmpeg -framerate 25 -i FRAME_%03d.jpg -c:v libx264 -r 30 output.mp4
		*/

		cv::imshow("YOLOv4", frame);
		if (cv::waitKey(1) == 'q')
			break;
	}
	cap.release();
	cv::destroyAllWindows();
	return(0);
}
